<?php

namespace Flood\Component\Json;

class Json {

    public static $debug = false;

    public static function encode($value, $options = 0, $depth = 512) {
        $return_val = json_encode($value, $options, $depth);
        if(static::lastError() != JSON_ERROR_NONE) {
            $return_val = false;
        }

        return $return_val;
    }

    public static function decode($json, $assoc = false, $depth = 512, $options = 0) {
        $return_val = json_decode($json, $assoc, $depth, $options);
        if(static::lastError() != JSON_ERROR_NONE) {
            error_log('Json.decode: ' . static::lastError());

            return null;
        }

        return $return_val;
    }

    /**
     * @param string $filename absolute path to the file
     * @param bool   $assoc
     * @param int    $depth
     * @param int    $options
     *
     * @return bool|mixed
     */
    public static function decodeFile($filename, $assoc = false, $depth = 512, $options = 0) {
        return static::decode(file_get_contents($filename), $assoc, $depth, $options);
    }

    /**
     * @param bool $as_code
     *
     * @return bool|int false when no error has happend at last, true when error happened, when as_code: php json error code
     */
    public static function lastError($as_code = false) {
        if($as_code) {
            return json_last_error();
        } else {
            if(json_last_error() != JSON_ERROR_NONE) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function lastErrorMsg() {
        return json_last_error_msg();
    }
}
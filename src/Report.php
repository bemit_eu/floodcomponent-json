<?php

namespace Flood\Component\Json;


class Report {
    protected $active = ['name' => []];
    protected $report = [
        'item' => [],
    ];

    /**
     * @param null|\Flood\Component\Json\Report $report a reports data will be merged into this reports data
     */
    public function add($report) {
        if(!empty($report->getItem())) {
            $this->report['item'] = array_merge($this->report['item'], $report->getItem());
        }
    }

    /**
     * @param array|null $item
     */
    public function addItem($item) {
        if(is_array($item)) {
            $this->report['item'] = \Flood\Component\Func\Array_::merge_recursive_distinct($this->report['item'], $item);
        }
    }

    /**
     * @return array
     */
    public function getItem() {
        return $this->report['item'];
    }

    public function getSchemeData($ident) {
        if(isset($this->report['item'][$ident])) {
            return $this->report['item'][$ident]['schema'];
        }
        return null;
    }

    public function getItemName($ident) {
        if(isset($this->report['item'][$ident])) {
            return $this->report['item'][$ident]['name'];
        }
        return null;
    }

    /**
     * @param $name
     */
    public function addActiveName($name) {
        $this->active['name'][] = $name;
    }

    /**
     * Removes the last active element, needed when the schema parser goes from an array depper to one higher
     */
    public function toActiveNameParent() {
        array_pop($this->active['name']);
    }

    /**
     * Removes the last active element, needed when the schema parser goes from an array depper to one higher
     *
     * @return string
     */
    public function getActiveNameLast() {
        return array_values(array_slice($this->active['name'], -1))[0];
    }

    /**
     * Removes the last active element, needed when the schema parser goes from an array depper to one higher
     *
     * @return string
     */
    public function getActiveName($imploded = false) {
        if(!$imploded) {
            return $this->active['name'];
        } else {
            return implode('.', $this->active['name']);
        }
    }
}
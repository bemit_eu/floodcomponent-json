<?php

namespace Flood\Component\Json;

class Schema {
    protected static $valid;

    protected $array;
    protected $boolean;
    protected $number;
    protected $string;

    public function __construct() {
        $this->array = new TypeArray();
        $this->string = new TypeString();
        $this->boolean = new TypeBoolean();
        $this->number = new TypeNumber();
    }

    public function validateFile($json_path, $schema_path) {
        self::$valid = true;
        //var_dump($this->validate(JSON::decodeFile($json_path, true), \Flood\Filesystem\File\Read::getContents($schema_path)));

        /*var_dump($this->isValid());
        var_dump($this->getError());*/
        return $this->validate(Json::decodeFile($json_path, true), \Flood\Filesystem\File\Read::getContents($schema_path));
    }

    /**
     * Validates data againt a json scheme
     *
     * @param array|int|float|string|bool|object|Json $data   the data that should be validated (yeah I know that I could have used mixed, but this was funnier)
     * @param array|Json                              $schema the json schema that the data will be validated against
     * @param null|\Flood\Component\Json\Report       $report
     *
     * @return \Flood\Component\Json\Report report of validation
     */
    public function validate($data, $schema = [],
                             $report = null) {
        if(null === $report) {
            $report = new Report();
        }

        $rt = [
            'active'   => [
                'name' => '',
            ],
            'item'     => [],
            'php_data' => [],
        ];
        /*var_dump($data);
        var_dump($schema);*/
        if(!is_array($schema)) {
            //$report['data_php'][]
            $schema = Json::decode($schema, true);
        }
        //var_dump(key($schema));
        if(isset($schema['type'])) {
            switch($schema['type']) {
                case 'object':
                case 'array':
                    /*if(is_object($data)) {
                        echo 'object';
                    }*/
                    //echo 'case \'object\':case \'array\':' . "\r\n";
                    $report->add($this->array->validate($data, $schema, $report));
                break;

                case 'string':
                    //echo 'case \'string\':' . "\r\n";
                    $report->add($this->string->validate($data, $schema, $report));
                break;

                case 'boolean':
                    //echo 'case \'boolean\':' . "\r\n";
                    $report->add($this->boolean->validate($data, $schema, $report));
                break;

                case 'number':
                    //echo 'case \'number\':' . "\r\n";
                    $report->add($this->number->validate($data, $schema, $report));
                break;
            }
        }
        //$this->validateType($scheme['type']);
        //var_dump($data);
        //echo '#';
        //var_dump($schema);
        //var_dump($report);

        return $report;
    }

    /**
     * generates a new property with default value
     *
     * @param $schema
     *
     * @return array|bool|int|null|string
     */
    public function generatePropertyRequired($schema) {
        $report = null;
        switch($schema['type']) {
            /*case 'object':
                $report = (object)[];
            break;*/

            case 'object':
            case 'array':
                $report['php_data'] = TypeArray::getPropertyEmpty();
            break;

            case 'string':
                $report['php_data'] = TypeString::getPropertyEmpty();
            break;

            case 'boolean':
                $report['php_data'] = TypeBoolean::getPropertyEmpty();
            break;

            case 'number':
                $report['php_data'] = TypeNumber::getPropertyEmpty();
            break;
        }
        if(isset($schema['default'])) {
            $report['php_data'] = $schema['default'];
        }

        return $report;
    }

    public function getDefault($key) {

    }

    public function isValid() {

    }

    public function getError() {

    }
}
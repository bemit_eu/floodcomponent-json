<?php

namespace Flood\Component\Json;


class TypeArray {
    protected $schema;

    public function __construct($array = null, $schema = null) {
    }

    /**
     * @param                    $array
     * @param                    $schema
     * @param \Flood\Component\Json\Report $report
     *
     * @return \Flood\Component\Json\Report
     */
    public function validate($array, $schema, $report) {
        $this->schema = new Schema();
        //$report = [];
        if(is_array($array)) {
            // init data php array with index of active json
            //$report['data_schema']=

            /*var_dump($array);
            echo 'key($array)';
            var_dump(key($array));*/
            //$report['active']['name'] = implode('.', explode('.', $report['active']['name'])) . '.' . key($array);
            //$report['data_php'][key($array)] = [];
            foreach($schema['properties'] as $schema_property_key => $schema_property) {
                if(array_key_exists($schema_property_key, $array)) {
                    // when the property key exists in json array check the type and more
                    $report->addActiveName($schema_property_key);
                    //$report['active']['name'][] = $schema_property_key;
                    //$report['active']['name'] = implode('.', explode('.', $report['active']['name'])) . '.' . $schema_property_key;
                    //var_dump($this->schema->validate($array[$schema_property_key], $schema_property, $report));
                    //echo "validate";
                    //$report_sub = array_merge($this->schema->validate($array[$schema_property_key], $schema_property, $report), $report_sub);
                    $report->add($this->schema->validate($array[$schema_property_key], $schema_property, $report));
                    //$report['item'] = array_merge($this->schema->validate($array[$schema_property_key], $schema_property, $report)['item'], $report_sub_tmp['item']);
                    $report->toActiveNameParent();
                } else if(is_array($schema['required'])) {
                    // when the property key exists not in json rray
                    // and there are required properties
                    // check if required
                    if(in_array($schema_property_key, $schema['required'])) {
                        // the property is not in json data but required
                        // generate a php data property and add to report
                        //$report_sub = array_merge($this->schema->generatePropertyRequired($schema_property), $report_sub);
                        //$report['item'][$report['active']['name']]['required'] = true;
                    }
                }
            }

            /*$report['item'][$report['active']['name']]['name'] = key($array);
            if(isset($schema['type'])) {
                $report['item'][$report['active']['name']]['type'] = $schema['type'];
            }
            if(isset($schema['description'])) {
                $report['item'][$report['active']['name']]['description'] = $schema['description'];
            }
            if(isset($schema['default'])) {
                $report['item'][$report['active']['name']]['default'] = $schema['default'];
            }
            $report = array_merge($report, $report_sub);*/
        }
        return $report;
    }

    public static function getPropertyEmpty() {
        return [];
    }
}
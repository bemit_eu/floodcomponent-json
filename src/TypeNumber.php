<?php

namespace Flood\Component\Json;


class TypeNumber {
    protected $schema;

    public function __construct($bool = null, $schema = null) {
    }

    /**
     * @param                    $number
     * @param                    $schema
     * @param \Flood\Component\Json\Report $report
     *
     * @return \Flood\Component\Json\Report
     */
    public function validate($number, $schema, $report) {
        $this->schema = new Schema();
        if(is_numeric($number)) {
            // init data php array with index of active json
            //$report['data_schema']=
            //$report[$key]['data_php'] = 0;
        }

        /*if(in_array($key, $schema['required'])) {
            // the property is not in json data but required
            // generate a php data property and add to report
            //$report[$key]['php_data'] = $this->schema->generatePropertyRequired($schema);
        }*/

        $report->addItem([
            $report->getActiveName(true) => [
                'name'   => $report->getActiveNameLast(),
                'schema' => $schema,
            ],
        ]);

        return $report;
    }

    public static function getPropertyEmpty() {
        return 0;
    }
}
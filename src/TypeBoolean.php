<?php

namespace Flood\Component\Json;


class TypeBoolean {
    protected $schema;

    public function __construct($bool = null, $schema = null) {
    }

    /**
     * @param                    $bool
     * @param                    $schema
     * @param \Flood\Component\Json\Report $report
     *
     * @return \Flood\Component\Json\Report
     */
    public function validate($bool, $schema, $report) {
        $this->schema = new Schema();
        if(is_bool($bool)) {
            // init data php array with index of active json
            //$report['data_schema']=
            //$report['data_php'][$key] = false;
        }

        /*if(in_array($key, $schema['required'])) {
            // the property is not in json data but required
            // generate a php data property and add to report
            //$report['php_data'][$key] = $this->schema->generatePropertyRequired($schema);
        }*/

        $report->addItem([
            $report->getActiveName(true) => [
                'name'   => $report->getActiveNameLast(),
                'schema' => $schema,
            ],
        ]);
        return $report;
    }

    public static function getPropertyEmpty() {
        return false;
    }
}
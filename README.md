# README #

Flood Component

`composer require flood/component-json`  

# Licence
This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

# Information, Support and Documentation
More informations, contacts and support for Flood could be found under <https://flood.bemit.codes> the documentation is located at <https://help.flood.bemit.codes>.

# Made by
Michael Becker, michael@bemit.codes